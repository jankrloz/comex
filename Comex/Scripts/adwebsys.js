﻿jQuery(document).ready(function () {
    if (jQuery(document).width() > 1024) {
        scrolleffect();
        $('.circle, #head-menu li').mouseover(function () {
            //$(this).addClass("animated fadeIn");
            $(this).css({
                'opacity': '0.8'
            });
        });

        $('.circle, #head-menu li').mouseleave(function () {
            $(this).css('opacity', '1');
        });
    }
});