﻿// Requiere de animate.css
// Agregar clase .scrollefect a elementos que deseé aplicar el efecto

function scrolleffect() {
    jQuery(".scrolleffect").appear();
    jQuery(".scrolleffect").on('appear',
        function () {
            var effect;
            if (jQuery(this).attr('animation-class') !== undefined && jQuery(this).attr('animation-delay-time') !== undefined) {

                if (!jQuery(this).hasClass('animated')) {
                    effect = 'animated ' + jQuery(this).attr('animation-class');
                    var time = jQuery(this).attr('animation-delay-time');
                    var element = this;
                    setTimeout(function () {
                        jQuery(element).addClass(effect);
                    }, time);
                }
            } else if (jQuery(this).attr('animation-class')) {
                if (!jQuery(this).hasClass('animated')) {
                    effect = 'animated ' + jQuery(this).attr('animation-class');
                    jQuery(this).addClass(effect);
                }
            } else {
                if (!jQuery(this).hasClass('animated')) {
                    jQuery(this).addClass("animated fadeInUp");
                }
            }
        }
    );
}