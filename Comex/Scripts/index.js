﻿$(document).ready(function () {

    $('.carrusel-1').slick({
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 6,
        slidesToScroll: 3,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 1500,
        infinite: true,
        easing: 'easeOutQuart',
        swipeToSlide: true,
        responsive: [
          {
              breakpoint: 992,
              settings: {
                  arrows: false,
                  centerMode: true,
                  centerPadding: '40px',
                  slidesToShow: 3
              }
          },
          {
              breakpoint: 480,
              settings: {
                  arrows: false,
                  centerMode: true,
                  centerPadding: '40px',
                  slidesToShow: 1
              }
          }
        ]
    });
});