﻿Imports System.Web.Optimization

Public Module BundleConfig
    ' Para obtener más información sobre las uniones, visite http://go.microsoft.com/fwlink/?LinkId=301862
    Public Sub RegisterBundles(ByVal bundles As BundleCollection)

        bundles.Add(New ScriptBundle("~/bundles/jquery").Include(
                    "~/Scripts/jquery-{version}.js"))

        bundles.Add(New ScriptBundle("~/bundles/jqueryval").Include(
                    "~/Scripts/jquery.validate*"))

        ' Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
        ' preparado para la producción y podrá utilizar la herramienta de compilación disponible en http://modernizr.com para seleccionar solo las pruebas que necesite.
        bundles.Add(New ScriptBundle("~/bundles/modernizr").Include(
                    "~/Scripts/modernizr-*"))

        bundles.Add(New ScriptBundle("~/bundles/bootstrap").Include(
                  "~/Scripts/bootstrap.js",
                  "~/Scripts/respond.js"))

        bundles.Add(New StyleBundle("~/Content/css").Include(
                  "~/Content/bootstrap.css",
                  "~/Content/site.css"))



        bundles.Add(New ScriptBundle("~/bundles/adwebsys").Include("~/Scripts/adwebsys.js"))


        bundles.Add(New StyleBundle("~/Content/index").Include(
                    "~/Content/slick/slick.css",
                    "~/Content/index.css"))

        bundles.Add(New StyleBundle("~/Content/manualidades").Include("~/Content/manualidades.css"))
        bundles.Add(New StyleBundle("~/Content/trajes").Include("~/Content/trajes.css"))

        bundles.Add(New ScriptBundle("~/bundles/index").Include(
                    "~/Content/slick/slick.min.js",
                    "~/Scripts/index.js"))

        ' Para la depuración, establezca EnableOptimizations en false. Para obtener más información,
        ' visite http://go.microsoft.com/fwlink/?LinkId=301862
        BundleTable.EnableOptimizations = True
    End Sub
End Module

