﻿Imports System.Web.Optimization

Public Class MvcApplication
    Inherits System.Web.HttpApplication

    Protected Sub Application_BeginRequest(sender As [Object], e As EventArgs)
        Dim passed As Boolean = True
        If Context.Request.Url.AbsoluteUri.ToLower().EndsWith(".htm") Or Context.Request.Url.AbsoluteUri.ToLower().EndsWith(".swf") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/flash/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/cotizaciones/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/images/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/js/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/less/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("fraganceloader.aspx") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("jpg") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("png") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/css/") Or Context.Request.Url.AbsoluteUri.Contains("/images/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/client/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/seccion/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/services/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/scss/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/fonts/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/upload/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/scripts/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/content/") Then
            passed = False
        End If

        If passed Then
            If Context.Request.Url.AbsoluteUri.ToLower().EndsWith("comex.html") Then
                Context.RewritePath("Home/Index", True)
                passed = False
            ElseIf Context.Request.Url.AbsoluteUri.ToLower().EndsWith("manualidades.html") Then
                Context.RewritePath("Home/Manualidades", True)
                passed = False
            ElseIf Context.Request.Url.AbsoluteUri.ToLower().EndsWith("trajes-tipicos.html") Then
                Context.RewritePath("Home/Trajes", True)
                passed = False
            End If
        End If
    End Sub

    Protected Sub Application_Start()
        AreaRegistration.RegisterAllAreas()
        FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters)
        RouteConfig.RegisterRoutes(RouteTable.Routes)
        BundleConfig.RegisterBundles(BundleTable.Bundles)
    End Sub
End Class
