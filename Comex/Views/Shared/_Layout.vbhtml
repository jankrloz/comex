﻿<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@ViewBag.Title</title>
    @Styles.Render("~/Content/css")
    @Scripts.Render("~/bundles/modernizr")

    <link href="~/Content/animate.css" rel="stylesheet">

    @RenderSection("othercss", required:=False)

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="http://use.edgefonts.net/source-sans-pro.js"></script>

    <link rel="shortcut icon" type="image/png" href="~/Images/logo.png">
</head>
<body>

    <div id="fb-root"></div>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&appId=114065665271745&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <header>
        <div class="container animated fadeIn">
            <div class="row">
                <div id="logo" class="col-md-3 col-lg-2">
                    <center><a href="~/comex.html"><img src="~/Images/logo.png" alt="logo" /></a></center>
                </div>
                <div class="col-md-9 col-lg-10">
                    <div class="row">
                        <div id="head-phone" class="col-sm-6 col-lg-4 col-lg-offset-5">
                            <p>
                                Teléfono: 01 (55) 5597-5100<br />
                                LADA sin Costo: 01800 5551-715
                            </p>
                        </div>
                        <div id="head-social" class="col-sm-6 col-lg-3">
                            <div class="circle"><center><i class="fa fa-phone"></i></center></div>
                            <div class="circle"><center><i class="fa fa-pencil"></i></center></div>
                            <div class="circle"><center><i class="fa fa-comments-o"></i></center></div>
                            <div class="circle"><center><i class="fa fa-facebook"></i></center></div>
                            <div class="circle"><center><i class="fa fa-twitter"></i></center></div>
                            <div class="circle"><center><i class="fa fa-google-plus"></i></center></div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="head-menu" class="col-xs-12">

                            <div class="dropdownmenu">
                                <span>Menú</span>
                                <i class="fa fa-bars"></i>
                            </div>
                            <ul>
                                <li id="head-menu-1"><a href="~/comex.html">Inicio</a></li>
                                <li id="head-menu-2"><a href="">Quiénes Somos</a></li>
                                <li id="head-menu-3"><a href="">Catálogo</a></li>
                                <li id="head-menu-4"><a href="">Dónde Comprar</a></li>
                                <li id="head-menu-5"><a href="~/trajes-tipicos.html">Trajes Típicos</a></li>
                                <li id="head-menu-6"><a href="~/manualidades.html">Manualidades</a></li>
                                <li id="head-menu-7"><a href="">Venta en Línea</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    @RenderBody()

    <footer>
        <div id="footer-sections">
            <div class="container">
                <div class="row">
                    <div id="footer-section-1" class="col-md-3 col-sm-6">
                        <p class="footer-title">INFORMACIÓN CORPORATIVA</p>
                        <ul>
                            <li><a href="">Quiénes Somos</a></li>
                            <li><a href="">Fabricantes</a></li>
                            <li><a href="">Distribuidores</a></li>
                            <li><a href="">Dónde Comprar</a></li>
                        </ul>
                    </div>
                    <div id="footer-section-2" class="col-md-3 col-sm-6">
                        <p class="footer-title">INFORMACIÓN COMERCIAL</p>
                        <ul>
                            <li><a href="">Catálogo</a></li>
                            <li><a href="~/trajes-tipicos.html">Trajes Típicos</a></li>
                            <li><a href="~/manualidades.html">Manualidades</a></li>
                            <li><a href="">Listones</a></li>
                            <li><a href="">Accesorios</a></li>
                        </ul>
                    </div>
                    <div id="footer-section-3" class="col-md-3 col-sm-6">
                        <p class="footer-title">OFICINAS CORPORATIVAS</p>
                        <p>
                            Compañía Mexicana Textil
                            <br />Sándalo No.58
                            <br />Col. Santa María Insurgentes
                            <br />C.P. 06430 México D.F.
                            <br /><br />Tel: (55) 5597-5100
                            <br />Fax: (55) 5597-3304
                            <br />Conmutador: (55) 5597-5100
                        </p>
                    </div>
                    <div id="footer-section-4" class="col-md-3 col-sm-6">
                        <div class="row">
                            <div class="col-xs-12">
                                <p>COMPARTE</p>
                                <div class="circle"><center><i class="fa fa-facebook"></i></center></div>
                                <div class="circle"><center><i class="fa fa-twitter"></i></center></div>
                                <div class="circle"><center><i class="fa fa-google-plus"></i></center></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <p>SÍGUENOS</p>
                                <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <p>VISÍTANOS</p>
                                <div class="circle"><center><i class="fa fa-facebook"></i></center></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <p id="footer-privacy">Aviso de Privacidad</p>
                        <p id="footer-company-name">
                            Compañía Mexicana Textil S.A. de C.V. © 2014 &nbsp;&nbsp;&nbsp;&nbsp; Diseño y Posicionamiento Web Por <a href="http://www.adwebsys.com">Adwebsys</a>
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </footer>

    @Scripts.Render("~/bundles/jquery")
    @Scripts.Render("~/bundles/bootstrap")

    @Scripts.Render("~/bundles/adwebsys")

    <script src="~/Scripts/jquery.appear.js"></script>
    <script src="~/Scripts/scrolleffect.js"></script>

    @RenderSection("scripts", required:=False)

    <script>
        $('.dropdownmenu').click(function () {
            $('header #head-menu ul').slideToggle('fast');
        });

        $('.footer-title').click(function () {
            $(this).next().slideToggle();
        });
    </script>

</body>
</html>
