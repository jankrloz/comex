﻿@Code
    ViewData("Title") = "Inicio"
End Code

@Section othercss

    @Styles.Render("~/Content/index")

End Section

<div id="slider" class="animated fadeIn">
    <img src="~/Images/Banners/1.jpg" alt="slider" />
</div>

<h1><span>Listones, cintas y elásticos</span><br />para todos los usos y ocaciones</h1>

<div class="carrusel-1">
    <div class="item-ca">
        <img src="~/Images/1erCarrusel/florales.jpg" alt="florales" />
        <p>FLORALES</p>
    </div>
    <div class="item-ca">
        <img src="~/Images/1erCarrusel/confeccion.jpg" alt="confeccion" />
        <p>CONFECCIÓN</p>
    </div>

    <div class="item-ca">
        <img src="~/Images/1erCarrusel/ropa_intima.jpg" alt="ropa intima" />
        <p>ROPA ÍNTIMA</p>
    </div>
    <div class="item-ca">
        <img src="~/Images/1erCarrusel/vacio.jpg" alt="" />
        <p>&nbsp;</p>
    </div>
    <div class="item-ca">
        <img src="~/Images/1erCarrusel/peinado.jpg" alt="peinado" />
        <p>PEINADO</p>
    </div>
    <div class="item-ca">
        <img src="~/Images/1erCarrusel/manualidades.jpg" alt="manualidades" />
        <p>MANUALIDADES</p>
    </div>
    <div class="item-ca">
        <img src="~/Images/1erCarrusel/ceremonia.jpg" alt="ceremonia" />
        <p>CEREMONIA</p>
    </div>
</div>

<a href="~/manualidades.html">
    <div id="manualidades" class="row scrolleffect alpha">
        <div id="manual-text" class="col-md-6">
            <p class="title">
                MANUALIDADES
            </p>
            <p class="text scrolleffect alpha col-lg-10 col-lg-offset-2">
                En esta sección ponemos a su disposición, algunas ideas que
                esperamos sean de su agrado y que le permitirán en forma fácil
                crear diferentes manualidades para decorar su evento, hogar
                o negocio que requiera su toque personal.
            </p>
            <i class="fa fa-caret-right"></i>
        </div>
        <div id="manual-imgs" class="col-md-6">
            <div class="img-div scrolleffect alpha"><center><img src="~/Images/vela_manua.png" alt="" /></center></div>
            <div class="img-div scrolleffect alpha"><center><img src="~/Images/regalo_manua.png" alt="" /></center></div>
            <div class="img-div scrolleffect alpha"><center><img src="~/Images/monito_manua.png" alt="" /></center></div>
        </div>
    </div>
</a>

<a href="~/trajes-tipicos.html">
    <div id="trajes" class="row scrolleffect alpha">
        <div id="trajes-imgs" class="col-md-6">
            <div class="img-div scrolleffect alpha"><center><img src="~/Images/muneca1.png" alt="" /></center></div>
            <div class="img-div scrolleffect alpha"><center><img src="~/Images/muneca2.png" alt="" /></center></div>
            <div class="img-div scrolleffect alpha"><center><img src="~/Images/muneca3.png" alt="" /></center></div>
        </div>
        <div id="trajes-text" class="col-md-6 scrolleffect alpha">
            <p class="title">
                TRAJES TÍPICOS
            </p>
            <p class="text scrolleffect alpha col-lg-10">
                Es de todos sabido que México es un país rico en Folklore y Tradiciones,
                y desde hace más de 100 años COMEX ha formado y enriquecido ese Folklore y Tradiciones
                con sus productos.
            </p>
            <i class="fa fa-caret-left"></i>
        </div>
    </div>
</a>

<h2><span>Ya Tenemos Venta en Línea</span><br />Por Caja Surtida</h2>
<div class="row carrusel-2 scrolleffect alpha">
    <div class="col-xs-4 col-sm-4 col-md-2"><img src="~/Images/Venta linea/zapatos2.jpg" alt="" /></div>
    <div class="col-xs-4 col-sm-4 col-md-2"><img src="~/Images/Venta linea/navidad.jpg" alt="" /></div>
    <div class="col-xs-4 col-sm-4 col-md-2"><img src="~/Images/Venta linea/manualidades.jpg" alt="" /></div>
    <div class="col-xs-4 col-sm-4 col-md-2"><img src="~/Images/Venta linea/listones_espa.jpg" alt="" /></div>
    <div class="col-xs-4 col-sm-4 col-md-2"><img src="~/Images/Venta linea/colorido_primavera.jpg" alt="" /></div>
    <div class="col-xs-4 col-sm-4 col-md-2"><img src="~/Images/Venta linea/canasta2.jpg" alt="" /></div>
</div>
<hr />
<div id="mapa" class="container">
    <div class="row">
        <div id="title" class="scrolleffect alpha col-xs-12">
            <p>
                Encuentre su Distribuidor <span>COMEX</span> más cercano
                <br /><span id="span2">siempre hay uno a su alcance</span>
            </p>
        </div>
    </div>
</div>
<div id="thumbs">
    <div class="container">
        <div class="row">
            <div id="thumb-fabr" class="col-sm-4 thumb scrolleffect alpha">
                <i class="fa fa-group scrolleffect alpha"></i>
                <p class="title scrolleffect alpha">Fabricantes</p>
                <p class="text scrolleffect alpha">
                    Si está interesado en adquirir nuestros productos
                    o requiere una cotización por favor envíe sus datos
                    y de inmediato nos comunicaremos con usted.
                </p>
            </div>
            <div id="thumb-dist" class="col-sm-4 thumb scrolleffect alpha">
                <i class="fa fa-truck scrolleffect alpha"></i>
                <p class="title scrolleffect alpha">Distribuidores</p>
                <p class="text scrolleffect alpha">
                    Si esta interesado en formar parte de nuestros distribuidores
                    o requiere una cotización por favor envíe sus datos
                    y de inmediato nos comunicaremos con usted.
                </p>
            </div>
            <div id="thumb-cyt" class="col-sm-4 thumb scrolleffect alpha">
                <i class="fa fa-map-marker scrolleffect alpha"></i>
                <p class="title scrolleffect alpha">Cytesa</p>
                <p class="text scrolleffect alpha">
                    Para cintas industriales y elásticas en
                    poliéster, nylon, algodón, polipropileno,
                    látex y licra en todos los anchos.
                    <br />Visite CYTESA
                </p>
            </div>
        </div>
    </div>
</div>
@Section scripts
    @Scripts.Render("~/bundles/index")
End Section
