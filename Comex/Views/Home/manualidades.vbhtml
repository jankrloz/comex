﻿@Code
    ViewData("Title") = "Manualidades"
End Code

@Section othercss

    <!-- Add fancyBox -->
    <link rel="stylesheet" href="~/Scripts/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />

    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <link rel="stylesheet" href="~/Scripts/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
    <link rel="stylesheet" href="~/Scripts/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />

    @Styles.Render("~/Content/manualidades")

End Section

<div id="banner" class="animated fadeIn">
    <center><img src="~/Images/Manualidades/banner_manualidades.jpg" alt="banner" /></center>
</div>

<div class="container" id="main">
    <div class="row">
        <div class="col-sm-6">
            <div>
                <p id="title" class="scrolleffect alpha">Manualidades</p>
                <p id="text" class="scrolleffect alpha">
                    En esta sección ponemos a su disposición, algunas ideas que
                    esperamos sean de su agrado y que le permitirán en forma fácil
                    crear diferentes manualidades para decorar su evento, hogar
                    o negocio que requiera su toque personal.
                    <br /><br />
                    Los Materiales necesarios para elaborar cualquiera de las manualidades
                    que se muestran, los puede encontrar en su Mercería favorita o si lo prefiere
                    ´de clic al botón "DONDE COMPRAR"
                </p>
            </div>
        </div>
        <div class="col-sm-6 scrolleffect alpha">
            <center>
                <img id="main-img" src="~/Images/Manualidades/img_manualidades.jpg" alt="manualidades" />
            </center>
        </div>
    </div>
</div>

<div id="galeria" class="container">
    <div class="row">
        <div class="col-xs-12" id="galeria-title">
            <p>Galería</p>
        </div>
        </div>
    <div class="row">
        <div class="col-xs-6 col-sm-3">
            <center>
                <div class="img-galeria scrolleffect alpha">
                    <img src="~/Images/Manualidades/Galeria/01.jpg" alt="galeria" />
                    <p class="img-title">Guante de Cocina</p>
                    <a href="~/Images/Manualidades/Galeria/01.jpg" class="fancybox" rel="manualidades">
                        <div class="img-button scrolleffect alpha">
                            <p>Ver Procedimiento</p>
                        </div>
                    </a>
                </div>
            </center>
        </div>
        <div class="col-xs-6 col-sm-3">
            <center>
                <div class="img-galeria scrolleffect alpha">
                    <img src="~/Images/Manualidades/Galeria/02.jpg" alt="galeria" />
                    <p class="img-title">Cubre Licuadora</p>
                    <a href="~/Images/Manualidades/Galeria/02.jpg" class="fancybox" rel="manualidades">
                        <div class="img-button scrolleffect alpha">
                            <p>Ver Procedimiento</p>
                        </div>
                    </a>
                </div>
            </center>
        </div>
        <div class="col-xs-6 col-sm-3">
            <center>
                <div class="img-galeria scrolleffect alpha">
                    <img src="~/Images/Manualidades/Galeria/03.jpg" alt="galeria" />
                    <p class="img-title">Toalla Facial</p>
                    <a href="~/Images/Manualidades/Galeria/03.jpg" class="fancybox" rel="manualidades">
                        <div class="img-button scrolleffect alpha">
                            <p>Ver Procedimiento</p>
                        </div>
                    </a>
                </div>
            </center>
        </div>
        <div class="col-xs-6 col-sm-3">
            <center>
                <div class="img-galeria scrolleffect alpha">
                    <img src="~/Images/Manualidades/Galeria/04.jpg" alt="galeria" />
                    <p class="img-title">Servilleta Oaxaqueña</p>
                    <a href="~/Images/Manualidades/Galeria/04.jpg" class="fancybox" rel="manualidades">
                        <div class="img-button scrolleffect alpha">
                            <p>Ver Procedimiento</p>
                        </div>
                    </a>
                </div>
            </center>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 col-sm-3">
            <center>
                <div class="img-galeria scrolleffect alpha">
                    <img src="~/Images/Manualidades/Galeria/01.jpg" alt="galeria" />
                    <p class="img-title">Guante de Cocina</p>
                    <a href="~/Images/Manualidades/Galeria/01.jpg" class="fancybox" rel="manualidades">
                        <div class="img-button scrolleffect alpha">
                            <p>Ver Procedimiento</p>
                        </div>
                    </a>
                </div>
            </center>
        </div>
        <div class="col-xs-6 col-sm-3">
            <center>
                <div class="img-galeria scrolleffect alpha">
                    <img src="~/Images/Manualidades/Galeria/02.jpg" alt="galeria" />
                    <p class="img-title">Cubre Licuadora</p>
                    <a href="~/Images/Manualidades/Galeria/02.jpg" class="fancybox" rel="manualidades">
                        <div class="img-button scrolleffect alpha">
                            <p>Ver Procedimiento</p>
                        </div>
                    </a>
                </div>
            </center>
        </div>
        <div class="col-xs-6 col-sm-3">
            <center>
                <div class="img-galeria scrolleffect alpha">
                    <img src="~/Images/Manualidades/Galeria/03.jpg" alt="galeria" />
                    <p class="img-title">Toalla Facial</p>
                    <a href="~/Images/Manualidades/Galeria/03.jpg" class="fancybox" rel="manualidades">
                        <div class="img-button scrolleffect alpha">
                            <p>Ver Procedimiento</p>
                        </div>
                    </a>
                </div>
            </center>
        </div>
        <div class="col-xs-6 col-sm-3">
            <center>
                <div class="img-galeria scrolleffect alpha">
                    <img src="~/Images/Manualidades/Galeria/04.jpg" alt="galeria" />
                    <p class="img-title">Servilleta Oaxaqueña</p>
                    <a href="~/Images/Manualidades/Galeria/04.jpg" class="fancybox" rel="manualidades">
                        <div class="img-button scrolleffect alpha">
                            <p>Ver Procedimiento</p>
                        </div>
                    </a>
                </div>
            </center>
        </div>
    </div>
</div>


@Section scripts

    <script type="text/javascript" src="~/Scripts/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script type="text/javascript" src="~/Scripts/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <script type="text/javascript" src="~/Scripts/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript" src="~/Scripts/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".fancybox").fancybox();
        });
    </script>    

End Section