﻿@Code
    ViewData("Title") = "Trajes Típicos"
End Code

@Section othercss

    <!-- Add fancyBox -->
    <link rel="stylesheet" href="~/Scripts/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    
    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <link rel="stylesheet" href="~/Scripts/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
    <link rel="stylesheet" href="~/Scripts/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />

    @Styles.Render("~/Content/trajes")

End Section

<div id="banner">
    <center><img src="~/Images/Trajes/banner_trajes.jpg" alt="banner" /></center>
</div>

<div class="container" id="main">
    <div class="row">
        <div class="col-sm-6">
            <div>
                <p id="title" class="scrolleffect alpha">Trajes Típicos</p>
                <p id="text" class="scrolleffect alpha">
                    Es de todos sabido que México es un país rico en  Folklore y Tradiciones,
                    y desde hace más de 100 años COMEX ha formado y enriquecido ese Folklore y
                    Tradiciones con sus productos.
                    <br /><br />
                    Siendo COMEX una empresa ORGULLOSAMENTE MEXICANA ponemos a su disposición
                    este HERMOSO CATÁLOGO DE TRAJES TÍPICOS MEXICANOS.
                    <br /><br />
                    Los materiales necesarios para elaborar cualquiera de los trajes típicos
                    aquí presentados, los puede encontrar en su Mercería favorita o si lo
                    prefiere de click al botón "DONDE COMPRAR".
                </p>
            </div>
        </div>
        <div class="col-sm-6 scrolleffect alpha">
            <center>
                <img id="main-img" src="~/Images/Trajes/img_trajes.jpg" alt="manualidades" />
            </center>
        </div>
    </div>
</div>

<div id="galeria" class="container">
    <div class="row">
        <div class="col-xs-12" id="galeria-title">
            <p>Galería</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 col-md-4">
            <div id="img1" class="img-div scrolleffect alpha">
                <a href="~/Images/Trajes/Galeria/Zoom/01.jpg" class="fancybox" rel="trajes">
                    <div class="img-detalle scrolleffect alpha">
                        <span>Ver Detalle</span>
                        <i class="fa fa-search"></i>
                    </div>
                </a>
                <div class="img-info scrolleffect alpha">
                    <span>Descargar Información</span>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-md-4">
            <div id="img2" class="img-div scrolleffect alpha">
                <a href="~/Images/Trajes/Galeria/Zoom/02.jpg" class="fancybox" rel="trajes">
                    <div class="img-detalle scrolleffect alpha">
                        <span>Ver Detalle</span>
                        <i class="fa fa-search"></i>
                    </div>
                </a>
                <div class="img-info scrolleffect alpha">
                    <span>Descargar Información</span>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-md-4">
            <div id="img3" class="img-div scrolleffect alpha">
                <a href="~/Images/Trajes/Galeria/Zoom/03.jpg" class="fancybox" rel="trajes">
                    <div class="img-detalle scrolleffect alpha">
                        <span>Ver Detalle</span>
                        <i class="fa fa-search"></i>
                    </div>
                </a>
                <div class="img-info scrolleffect alpha">
                    <span>Descargar Información</span>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-md-4">
            <div id="img4" class="img-div scrolleffect alpha">
                <a href="~/Images/Trajes/Galeria/Zoom/04.jpg" class="fancybox" rel="trajes">
                    <div class="img-detalle scrolleffect alpha">
                        <span>Ver Detalle</span>
                        <i class="fa fa-search"></i>
                    </div>
                </a>
                <div class="img-info scrolleffect alpha">
                    <span>Descargar Información</span>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-md-4">
            <div id="img5" class="img-div scrolleffect alpha">
                <a href="~/Images/Trajes/Galeria/Zoom/05.jpg" class="fancybox" rel="trajes">
                    <div class="img-detalle scrolleffect alpha">
                        <span>Ver Detalle</span>
                        <i class="fa fa-search"></i>
                    </div>
                </a>
                <div class="img-info scrolleffect alpha">
                    <span>Descargar Información</span>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-md-4">
            <div id="img6" class="img-div scrolleffect alpha">
                <a href="~/Images/Trajes/Galeria/Zoom/06.jpg" class="fancybox" rel="trajes">
                    <div class="img-detalle scrolleffect alpha">
                        <span>Ver Detalle</span>
                        <i class="fa fa-search"></i>
                    </div>
                </a>
                <div class="img-info scrolleffect alpha">
                    <span>Descargar Información</span>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-md-4">
            <div id="img7" class="img-div scrolleffect alpha">
                <a href="~/Images/Trajes/Galeria/Zoom/07.jpg" class="fancybox" rel="trajes">
                    <div class="img-detalle scrolleffect alpha">
                        <span>Ver Detalle</span>
                        <i class="fa fa-search"></i>
                    </div>
                </a>
                <div class="img-info scrolleffect alpha">
                    <span>Descargar Información</span>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-md-4">
            <div id="img8" class="img-div scrolleffect alpha">
                <a href="~/Images/Trajes/Galeria/Zoom/08.jpg" class="fancybox" rel="trajes">
                    <div class="img-detalle scrolleffect alpha">
                        <span>Ver Detalle</span>
                        <i class="fa fa-search"></i>
                    </div>
                </a>
                <div class="img-info scrolleffect alpha">
                    <span>Descargar Información</span>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-md-4">
            <div id="img9" class="img-div scrolleffect alpha">
                <a href="~/Images/Trajes/Galeria/Zoom/09.jpg" class="fancybox" rel="trajes">
                    <div class="img-detalle scrolleffect alpha">
                        <span>Ver Detalle</span>
                        <i class="fa fa-search"></i>
                    </div>
                </a>
                <div class="img-info scrolleffect alpha">
                    <span>Descargar Información</span>
                </div>
            </div>
        </div>
    </div>
</div>


@Section scripts

    <script type="text/javascript" src="~/Scripts/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script type="text/javascript" src="~/Scripts/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <script type="text/javascript" src="~/Scripts/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript" src="~/Scripts/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".fancybox").fancybox();
        });
    </script>

End Section